#include "Complex.h"
#include <cmath>


Complex::Complex(double real, double img) : real_(real), img_(img) {}

double Complex::getReal() const {
    return real_;
}

double Complex::getImg() const {
    return img_;
}

double Complex::modulus() const {
    return sqrt(img_ * img_ + real_ * real_);
}

Complex operator+(const Complex &first, const Complex &second) {
    return Complex(first.real_ + second.real_, first.img_ + second.img_);
}

Complex operator+(const Complex &complex, double scalar) {
    return Complex(complex.real_ + scalar, complex.img_);
}

Complex operator+(double scalar, const Complex &complex) {
    return complex + scalar;
}
