#ifndef COMPLEX_COMPLEX_H
#define COMPLEX_COMPLEX_H



class Complex {
public:
    Complex(double real, double img);

    double getReal() const;

    double getImg() const;

    double modulus() const;

    friend Complex operator+(const Complex &first, const Complex &second);

    friend Complex operator+(const Complex &complex, double scalar);

    friend Complex operator+(double scalar, const Complex &complex);

//    friend Complex operator*(const Complex &first, const Complex &second);

private:
    double real_;
    double img_;

};


#endif //COMPLEX_COMPLEX_H
