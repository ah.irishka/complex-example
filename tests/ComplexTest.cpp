#include <gtest/gtest.h>
#include "Complex.h"

TEST(ComplexTest, init) {
    Complex c(1, 2);
    ASSERT_FLOAT_EQ(c.getReal(), 1);
    ASSERT_FLOAT_EQ(c.getImg(), 2);
}

TEST(ComplexTest, modulus) {
    Complex c(3, 4);
    ASSERT_FLOAT_EQ(c.modulus(), 5);
}

TEST(ComplexTest, addition) {
    Complex a(1, 2);
    Complex b(3, 4);
    Complex c = a + b;
    ASSERT_FLOAT_EQ(c.getReal(), 4);
    ASSERT_FLOAT_EQ(c.getImg(), 6);
}

TEST(ComplexTest, additionScalarLeft) {
    Complex a(1, 2);
    double b = 3;
    Complex c = b + a;
    ASSERT_FLOAT_EQ(c.getReal(), 4);
    ASSERT_FLOAT_EQ(c.getImg(), 2);
}

TEST(ComplexTest, additionScalarRight) {
    Complex a(1, 2);
    double b = 3;
    Complex c = a + b;
    ASSERT_FLOAT_EQ(c.getReal(), 4);
    ASSERT_FLOAT_EQ(c.getImg(), 2);
}