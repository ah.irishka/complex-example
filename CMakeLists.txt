cmake_minimum_required(VERSION 3.15)
project(complex)

set(CMAKE_CXX_STANDARD 14)

include_directories(src)

add_subdirectory(gtest)

add_executable(complex src/main.cpp src/Complex.cpp src/Complex.h)

add_executable(complex_test tests/main.cpp src/Complex.cpp src/Complex.h tests/ComplexTest.cpp)

target_link_libraries(complex_test gtest)